package com.example.exemplesimplederecyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Adapter, héritant de RecyclerView.Adapter, et qui nous permettra de faire le lien entre
 * la vue RecyclerView et l'activité princicipale.
 * <p>
 * Elle est utilise pour 'adapter' les données passées en paramètre au view holder associé.
 * <p>
 * Généralement, un Adapter se construit avec la référence d'une liste d'objets passée en
 * paramètre depuis le contrôleur.
 * <p>
 * On trouve trois méthodes importantes :
 * - onCreateViewHolder
 * - onBindViewHolder
 * - getItemCount
 */

public class GameAdapter extends RecyclerView.Adapter<GameViewHolder> {

    private final List<GameViewModel> gameViewModels;
    private RequestManager glide;

    private final Listener callback;

    public GameAdapter(RequestManager glide, Listener callback) {
        this.gameViewModels = new ArrayList<>();
        this.glide = glide;
        this.callback = callback;
    }

    // permet d'associer de nouvelles données à l'adapter
    public void bindViewModels(List<GameViewModel> gameViewModels) {
        this.gameViewModels.clear();
        this.gameViewModels.addAll(gameViewModels);
        notifyDataSetChanged();
    }


    /*
      Permet de créer un ViewHolder à partir du layout XML représentant chaque ligne de la
      RecyclerView.
      Méthode appelée par la recyclerview à sa construction, pour les premières lignes visibles.
      Comportement semblable au onCreate d'une activité.
    */
    @NonNull
    @Override
    public GameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.cardview_item_recyclerview, parent, false);
        return new GameViewHolder(view);
    }

    /*
       Appelée pour chacune des lignes visibles affichées dans le RecyclerView.
       C'est généralement ici que l'on met à jour leur apparence.
     */
    @Override
    public void onBindViewHolder(@NonNull GameViewHolder viewHolder, int position) {
        viewHolder.updateWithGame(this.gameViewModels.get(position), this.glide, this.callback);
    }


    /*
       Permet de retourner la taille de la liste d'objets, et ainsi indiquer à l'Adapter le nombre
       de lignes que peut contenir la RecyclerView
     */
    @Override
    public int getItemCount() {
        return this.gameViewModels.size();
    }

    /*
        Renvoie le jeu à la position donnée
     */
    public GameViewModel getGameViewModel(int position) {
        return this.gameViewModels.get(position);
    }

    /**
     * ------ pour la gestion du clic ------------
     * On crée une interface de callback pour propager le clic sur un morceau d'item (par exemple
     * sur l'icône de poubelle) vers l'activité
     */
    public interface Listener {
        void onClickDeleteButton(int position);

        void onClickTitle(int position);

        void onClickDescription(int position);

        void onLongClickTitle(int position);

        void onLongClickDescription(int position);

        void onClickCardView(int adapterPosition);
    }
}

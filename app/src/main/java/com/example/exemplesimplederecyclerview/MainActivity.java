package com.example.exemplesimplederecyclerview;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.example.exemplesimplederecyclerview.Utils.ItemClickSupport;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Petit exemple simple de RecyclerView qui affiche des données brutes (pas de base de données, ni
 * d'appel à une API).
 */

public class MainActivity extends AppCompatActivity implements GameAdapter.Listener {

    private List<GameViewModel> gameViewModelList;
    private RecyclerView recyclerView;
    private GameAdapter adapter;

    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);

        swipeRefreshLayout = findViewById(R.id.listeDesJeux);
        this.configureSwipeRefreshLayout();

        this.configureRecyclerView();

//        this.configureOnClickRecyclerView();
    }

    // ajout de la gestion du clic sur un item (une ligne) de la RecyclerView
//    private void configureOnClickRecyclerView() {
//        ItemClickSupport.addTo(recyclerView, R.layout.item_recyclerview)
//                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//                    @Override
//                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                        GameViewModel gameViewModel = adapter.getGameViewModel(position);
//                        Toast.makeText(MainActivity.this, "Vous avez cliqué sur  : " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
//                    }
//                });
//        ItemClickSupport.addTo(recyclerView, R.layout.item_recyclerview)
//                .setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
//                    @Override
//                    public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
//                        GameViewModel gameViewModel = adapter.getGameViewModel(position);
//                        Toast.makeText(MainActivity.this, "Vous avez cliqué longtemps sur  : " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
//                        return true;
//                    }
//                });
//    }


    private void configureRecyclerView() {
        this.gameViewModelList = DataGenerator.generateData();
        this.adapter = new GameAdapter(Glide.with(this), this);
        this.recyclerView.setAdapter(adapter);
        this.adapter.bindViewModels(DataGenerator.generateData());
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void configureSwipeRefreshLayout() {
        this.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // on définit ici ce qu'il faut faire quand on demande un 'pull to refresh'. Dans
                // le cas présent, on se contente d'afficher un toast et de :
                // - regénérer les datas,
                // - ou les mettre en ordre aléatoire
                // - ou ajouter la lste à elle-même

                // si besoin (mais pas ici) :
                // purger la liste : gameViewModelList.clear();

                // on récupère les 'nouvelles' données (au choix ici)
                // la même liste gameViewModelList :
                //      gameViewModelList = DataGenerator.generateData();
                // la même liste en ordre aléatoire :
                //      gameViewModelList = DataGenerator.generateData();
                //      Collections.shuffle(gameViewModelList, new Random());
                // la liste qui s'ajoute à elle-même (et en aléatoire) :
                //      gameViewModelList.addAll(DataGenerator.generateData());
                //      Collections.shuffle(gameViewModelList);

                gameViewModelList = DataGenerator.generateData();
                Collections.shuffle(gameViewModelList, new Random());
                Log.d("JC", String.valueOf(gameViewModelList));

                // on avertit l'adapter que les données ont changé pour qu'il fasse le nécessaire
                // pour mettre à jour la RecyclerView.
                adapter.bindViewModels(gameViewModelList);

                // on arrête l'animation du refresh (flèche tournante)
                swipeRefreshLayout.setRefreshing(false);

                Toast.makeText(MainActivity.this, "liste raffraîchie", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClickDeleteButton(int position) {
        GameViewModel gameViewModel = adapter.getGameViewModel(position);
        Toast.makeText(this, "Suppression de " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickTitle(int position) {
        GameViewModel gameViewModel = adapter.getGameViewModel(position);
        Toast.makeText(this, "Clic sur le titre de " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickDescription(int position) {
        GameViewModel gameViewModel = adapter.getGameViewModel(position);
        Toast.makeText(this, "Clic sur la description de " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLongClickTitle(int position) {
        GameViewModel gameViewModel = adapter.getGameViewModel(position);
        Toast.makeText(this, "Clic long sur le titre de " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLongClickDescription(int position) {
        GameViewModel gameViewModel = adapter.getGameViewModel(position);
        Toast.makeText(this, "Clic long sur la description de " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickCardView(int position) {
        GameViewModel gameViewModel = adapter.getGameViewModel(position);
        Toast.makeText(this, "Clic sur la card de " + gameViewModel.getTitle(), Toast.LENGTH_SHORT).show();
    }
}
package com.example.exemplesimplederecyclerview;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.RequestManager;

/**
 * Classe qui représente le layout (la vue) d'une ligne sous la forme d'un objet Java.
 * On lui passe une vue (une ligne) en paramètre pour la création, et un objet GameViewModel
 * pour l'affichage du contenu de la ligne
 */

public class GameViewHolder extends ViewHolder implements View.OnLongClickListener, View.OnClickListener {

    private TextView title, description;
    private ImageView gameImageView;
    private ImageButton imageButton;
    private GameAdapter.Listener callback;

    private CardView cardView;

    // on récupère tous les objets de la vue dont on a besoin par la suite pour la mise à jour de
    // l'affichage
    public GameViewHolder(@NonNull View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.cardview_title);
       // description = itemView.findViewById(R.id.description);
        gameImageView=itemView.findViewById(R.id.cardview_image);
       // imageButton=itemView.findViewById(R.id.btn_delete);
        cardView=itemView.findViewById(R.id.cardView);
    }

    // méthode pour mettre à jour l'affichage d'une ligne
    public void updateWithGame(GameViewModel gameViewModel, RequestManager glide, GameAdapter.Listener callback) {
        this.title.setText(gameViewModel.getTitle());
        //this.description.setText(gameViewModel.getDescription());
        //glide.load(gameViewModel.getImageUrl()).apply(RequestOptions.centerCropTransform()).into(gameImageView);
        glide.load(gameViewModel.getImageUrl()).centerCrop().into(gameImageView);

        // si on veut gérer le (long) clic sur chacun des éléments d'un item du RecyclerView, par
        // ex. clic sur poubelle, long clic sur image ou sur le titre, etc.
       // this.imageButton.setOnClickListener(this);
        //this.imageButton.setOnLongClickListener(this);

        this.title.setOnClickListener(this);
//        this.title.setOnLongClickListener(this);

        //this.description.setOnClickListener(this);
        //this.description.setOnLongClickListener(this);

        this.cardView.setOnClickListener(this);

        this.gameImageView.setOnClickListener(this);

        this.callback=callback;
    }

    @Override
    public void onClick(View v) {
        if(callback!=null){
            switch (v.getId()) {
                case R.id.cardview_title:
                    callback.onClickTitle(getAdapterPosition());
                    break;
                case R.id.description:
                    callback.onClickDescription(getAdapterPosition());
                    break;
                case R.id.btn_delete:
                    callback.onClickDeleteButton(getAdapterPosition());
                    break;
                case R.id.cardView:
                    callback.onClickCardView(getAdapterPosition());
                    break;
                case R.id.cardview_image:
                    callback.onClickCardView(getAdapterPosition());
                    break;
                default:
                    // nothing to do here
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(callback!=null){
            switch (v.getId()) {
                case R.id.title:
                    callback.onLongClickTitle(getAdapterPosition());
                    return true;
                case R.id.description:
                    callback.onLongClickDescription(getAdapterPosition());
                    return true;
                case R.id.btn_delete:
                    //callback.onLongClickDeleteButton(getAdapterPosition());
                    return true;
                default:
                    // do nothing here;
            }
        }
        return false;
    }
}
